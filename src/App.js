import './App.css'
import { useState } from "react";

function App() {
    const [add, setadd] = useState([]);

    const dobav = () => {
        const pr = prompt('аты жону');
        const pr2 = prompt('Канча сом');
        const pr3 = prompt('url картинки');
        if (pr != null && pr2 != null) {
            const amountInDollars = (pr2 / 84).toFixed(2)
            setadd(prevState => [...prevState, { name: pr, akchasy: `$${amountInDollars}`, img: pr3, originalAkchasy: `$${pr2}` }]);
        }
    };

    const Delet = index => {
        const newAdd = [...add];
        newAdd.splice(index, 1);
        setadd(newAdd);
    };

    const changeAkchasy = (index) => {
        const newAdd = [...add];
        const product = newAdd[index];
        product.akchasy = product.originalAkchasy;
        setadd(newAdd);
    };

    return (
        <>
            <div className="glavnyi">
                <h1>Продукты</h1>
                <button className="but" onClick={dobav}>
                    Добавить продукт
                </button>
            </div>
            <div className={'glavnyi1'}>

                {add.map((product, index) => {
                    return (
                        <div className="container">
                            {product.img ? (
                                <img src={product.img} alt="" className="img" />
                            ) : (
                                <img
                                    className="img"
                                    src="https://miro.medium.com/v2/resize:fit:1400/1*k_aR1GjU4tzt0vO7fJv0JQ.png"
                                    alt=""
                                />
                            )}
                            <h3 className="h3">{product.name}</h3>
                            <span onClick={() => changeAkchasy(index)}>{product.akchasy}</span>
                            <button className="button" onClick={() => Delet(index)}>
                                Удалить
                            </button>
                        </div>
                    );
                })}
            </div>
        </>
    );
}

export default App;
